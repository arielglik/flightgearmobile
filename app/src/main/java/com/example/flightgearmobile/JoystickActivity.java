package com.example.flightgearmobile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;

public class JoystickActivity extends AppCompatActivity implements JoystickView.JoystickListener {

    private ConnectClient connectClient;

    @Override
    public void onJoystickMoved(float xPercent, float yPercent, int id) {
        if (yPercent != 0) {
            yPercent *= -1;
        }

        Log.d("Main Method", "X percent: " + xPercent + ", Y percent: " + yPercent);
        connectClient.writeToServer(xPercent, yPercent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_joystick);
        JoystickView joystickView = new JoystickView(this);
        setContentView(joystickView);
        this.connectClient = null;

        Intent intent = getIntent();
        String ip = intent.getStringExtra("ip");
        String port = intent.getStringExtra("port");

        this.connectClient = new ConnectClient(ip, port);
        this.connectClient.execute();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.connectClient.closeSocket();
        this.connectClient = null;
    }
}

class ConnectClient extends AsyncTask<Void, Void, Void> {

    private String ip;
    private String port;
    private Socket socket;
    private OutputStream os;

    public ConnectClient(String ip, String port) {
        this.ip = ip;
        this.port = port;
        this.socket = null;
        this.os = null;
    }

    @Override
    protected Void doInBackground(Void... voids) {
        try {
            int portValue = Integer.parseInt(port);
            InetAddress serverAddr = InetAddress.getByName(ip);
            this.socket = new Socket(serverAddr, portValue);
            this.os = socket.getOutputStream();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void writeToServer(float x, float y) {
        String aileron = Float.toString(x);
        String elevator = Float.toString(y);
        String setAileron = "set /controls/flight/aileron " + aileron + "\r\n";
        String setElevator = "set /controls/flight/elevator " + elevator + "\r\n";

        try {
            os.write(setAileron.getBytes());
            os.flush();
            os.write(setElevator.getBytes());
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void closeSocket() {
        try {
            socket.close();
            os.close();
            this.socket = null;
            this.os = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}