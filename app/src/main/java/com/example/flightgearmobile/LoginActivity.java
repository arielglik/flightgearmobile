package com.example.flightgearmobile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    // Called when user taps the Connect button
    public void sendConnectValues(View view) {
        EditText ip = (EditText) findViewById(R.id.IP_Value);
        EditText port = (EditText) findViewById(R.id.Port_Value);
//        TextView textView = (TextView)findViewById(R.id.textView);
//        textView.setText("IP: " + editText.getText().toString());
        Intent intent = new Intent(this, JoystickActivity.class);
        String ipValue = ip.getText().toString();
        String portValue = port.getText().toString();
        intent.putExtra("ip", ipValue);
        intent.putExtra("port", portValue);
        startActivity(intent);
    }

}
