package com.example.flightgearmobile;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

public class JoystickView extends SurfaceView implements SurfaceHolder.Callback, View.OnTouchListener {

    private Paint bigCirclePaint;
    private Paint smallCirclePaint;
    private float bigCircleRadius;
    private float smallCircleRadius;
    private float middleX;
    private float middleY;
    private JoystickListener joystickListener;


    public JoystickView(Context context) {
        super(context);
        initialize(context);
    }

    public JoystickView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    public JoystickView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize(context);
    }

    public void initialize(Context context) {
        bigCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        bigCirclePaint.setColor(Color.GRAY);
        bigCirclePaint.setStyle(Paint.Style.FILL);
        smallCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        smallCirclePaint.setColor(Color.GREEN);
        smallCirclePaint.setStyle(Paint.Style.FILL);
        middleX = getWidth() / 2;
        middleY = getHeight() / 2;
        bigCircleRadius = Math.min(getWidth(), getHeight()) / 3;
        smallCircleRadius = Math.min(getWidth(), getHeight()) / 5;
        getHolder().addCallback(this);
        setOnTouchListener(this);
        if (context instanceof JoystickListener) {
            joystickListener = (JoystickListener) context;
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        if (view.equals(this)) {
            if (motionEvent.getAction() != motionEvent.ACTION_UP) {
                float displacement = (float) Math.sqrt((Math.pow(motionEvent.getX() - middleX, 2))
                        + Math.pow(motionEvent.getY() - middleY, 2));

                if (displacement < bigCircleRadius) {
                    drawJoystick(motionEvent.getX(), motionEvent.getY());
                    joystickListener.onJoystickMoved((motionEvent.getX() - middleX) / bigCircleRadius,
                            (motionEvent.getY() - middleY) / bigCircleRadius, getId());
                } else {
                    float ratio = bigCircleRadius / displacement;
                    float constrainedX = middleX + (motionEvent.getX() - middleX) * ratio;
                    float constrainedY = middleY + (motionEvent.getY() - middleY) * ratio;
                    drawJoystick(constrainedX, constrainedY);
                    joystickListener.onJoystickMoved((constrainedX - middleX) / bigCircleRadius,
                            (constrainedY - middleY) / bigCircleRadius, getId());
                }
            } else {
                drawJoystick(middleX, middleY);
                joystickListener.onJoystickMoved(0, 0, getId());
            }
        }
        return true;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        bigCircleRadius = Math.min(getWidth(), getHeight()) / 3;
        smallCircleRadius = Math.min(getWidth(), getHeight()) / 5;
        middleX = getWidth() / 2;
        middleY = getHeight() / 2;
    }

    public void drawJoystick(float x, float y) {
        if (getHolder().getSurface().isValid()) {
            Canvas canvas = this.getHolder().lockCanvas();
            canvas.drawColor(Color.rgb(3, 169, 244)/*, PorterDuff.Mode.CLEAR*/); // draw background
            canvas.drawCircle(middleX, middleY, bigCircleRadius, bigCirclePaint);
            canvas.drawCircle(x, y, smallCircleRadius, smallCirclePaint);
            getHolder().unlockCanvasAndPost(canvas);
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        drawJoystick(middleX, middleY);
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
    }

    public interface JoystickListener {
        void onJoystickMoved(float xPercent, float yPercent, int id);
    }
}
